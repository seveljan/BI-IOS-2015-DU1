Toto je domácí úkol z BI-iOS. Jsou to updatované soubory z projektu:
https://github.com/Dominoo/bi-ios-2015-04

dle zadání:
Upravte kod z cvičení č. 4 (větev second) následujícím způsobem

Přidejte komponentu UISwitch, která bude zapinat/vypinat pohyb UISlideru a UIStepperu. Tudiz i pohyb cele animace (stopne timer, pokud bezi)
Přidejte komponentu UILabel, která bude zobrazovat aktuální hodnotu amplitudy naší křivky
Přidejte komponentu UISegmentedControl, která bude mít 3 hodnoty „Red, Blue, Green“ a podle zvolené barvy se bude obarvovat naše křivka
Upravte gesture recognizer tak, aby se dalo hýbat pouze křivkou a ne jejim podkladem (upravte = musite ho aplikovat na jine view)
Nejak pěkně graficky sjednoťte
Sjednotte interni stavy Slideru a Stepperu pomoci pomocne promenne a vyuzijte Swift didSet konstrukci pro sjednocení.
Ukol se bude kontrolovat na 7. cvičení pokud máte jakékoliv dotazy pište na veseldom@fit.cvut.cz nebo raději pro rychlejší reakci na dominik.vesely@ackee.cz případně na 6. cvičení osobně

