//
//  PanelView.swift
//  bi-ios-recognizers
//
//  Created by Dominik Vesely on 03/11/15.
//  Copyright © 2015 Ackee s.r.o. All rights reserved.
//

import Foundation
import UIKit

class PanelView : UIView {
    
    var delegate : PanelViewDelegate?
    
    var onSliderChange : ((CGFloat) -> ())?
    
    weak var slider : UISlider!
    weak var stepper : UIStepper!
    weak var switcher : UISwitch!
    weak var viewValue : UILabel!
    weak var segCon : UISegmentedControl!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        backgroundColor = UIColor.lightGrayColor()
        
        
        let slider = UISlider()
        slider.minimumValue = 0
        slider.maximumValue = 15
        slider.addTarget(self, action: "sliderChanged:", forControlEvents: UIControlEvents.ValueChanged)
        
        addSubview(slider)
        self.slider = slider
        
        let stepper = UIStepper()
        stepper.minimumValue = 0;
        stepper.maximumValue = 15;
        stepper.stepValue = 0.5;
        stepper.addTarget(self, action: "stepperChanged:", forControlEvents: UIControlEvents.ValueChanged)
       
        addSubview(stepper)
        self.stepper = stepper
        
        let timer = NSTimer.scheduledTimerWithTimeInterval(1/30, target: self, selector: "fireTimer:", userInfo: nil, repeats: true)
        //timer.performSelector("invalidate", withObject: nil, afterDelay: 5)
        self.performSelector("invalidateTimer:", withObject: timer, afterDelay: 5)
        
        let switcher = UISwitch()
        switcher.on = true
        addSubview(switcher)
        self.switcher = switcher
        
        let viewValue = UILabel()
        addSubview(viewValue)
        self.viewValue = viewValue
        
        let items = ["Red", "Green", "Blue"]
        let segCon = UISegmentedControl(items: items)
        segCon.selectedSegmentIndex = 0
        segCon.addTarget(self, action: "segConChanged:", forControlEvents: UIControlEvents.ValueChanged)
        
        addSubview(segCon)
        self.segCon = segCon
        
      
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        self.slider.frame = CGRectMake(8, 8, CGRectGetWidth(self.bounds) - 16, 44);
        self.stepper.frame = CGRectMake(8, 8 + 44+8, CGRectGetWidth(self.bounds) - 16, 44);
        self.switcher.frame = CGRectMake(8+132, 8 + 44+8, CGRectGetWidth(self.bounds) - 16, 44)
        self.viewValue.frame = CGRectMake(8+132+88, 8 + 44+8, CGRectGetWidth(self.bounds) - 16, 44)
        self.segCon.frame = CGRectMake(8, 8 + 44+44+8, CGRectGetWidth(self.bounds) - 16, 44)
        
        
    }
    
    
    func invalidateTimer(timer: NSTimer) {
        timer.invalidate()
    }
    
    
    //MARK: Action
    func fireTimer(timer:NSTimer) {
        var value = self.slider.value
        if self.switcher.on {
            value += 0.01
            self.slider.value = value
            self.stepper.value = Double(value)
            self.viewValue.text = String(format: "%.2f",value)
            sliderChanged(self.slider)
            stepperChanged(self.stepper)
            
        } else {
        }
        
    }
    
    
    func sliderChanged(slider : UISlider) {
        delegate?.sliderDidChange(slider, panel: self)
        self.slider.value = Float(stepper.value)
        //stepperChanged(self.stepper)
    }
    
    func stepperChanged(stepper: UIStepper) {
        delegate?.stepperDidChange(stepper, panel: self)
        self.stepper.value = Double(slider.value)
        //sliderChanged(self.slider)
    }
    
    func segConChanged(segCon: UISegmentedControl) {
        delegate?.segConChanged(segCon, panel: self)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}

protocol PanelViewDelegate {
    
    func sliderDidChange(slider : UISlider, panel:PanelView)
    func stepperDidChange(stepper : UIStepper, panel:PanelView)
    func segConChanged(segCon : UISegmentedControl, panel:PanelView)

}
